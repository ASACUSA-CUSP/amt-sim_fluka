// Conversion of Fluka's output in root tree for digitalization
#include <iostream>
#include <cmath>
#include <chrono>
#include <cstdlib>
#include <complex>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <string>

#include "TROOT.h"
#include "TTree.h"
#include "TFile.h"
#include "TF1.h"

using namespace std;
using namespace std::chrono;


string particles_name(int id, Char_t name[10]){
    if(id <= -7 || id >=-12){
     sscanf("HeavyFrag.","%s",name);//04
    }
    if(id == -6){
     sscanf("Hefour","%s",name);//04
    }
    if(id == -5){
     sscanf("Hethree","%s",name);//3
    }
    if(id == -4){
     sscanf("Triton","%s",name);
    }
    if(id == -3){
      sscanf("Deu","%s",name);
    }
    if(id == -2){
     sscanf("HeavyIon","%s",name);
    }
    if(id == 0){
     sscanf("Ray","%s",name);
    }
    if(id == 1){
     sscanf("p","%s",name);
    }
    if(id == 2){
     sscanf("pbar","%s",name);
    }
    if(id == 3){
     sscanf("e","%s",name);
    }
    if(id == 4){
     sscanf("ebar","%s",name);
    }
    if(id == 5){
     sscanf("nu","%s",name);
    }
    if(id == 6){
     sscanf("nubar","%s",name);
    }
    if(id == 7){
     sscanf("ph","%s",name);
    }
    if(id == 8){
     sscanf("n","%s",name);
    }
    if(id == 9){
     sscanf("nbarplus","%s",name);//+
    }
    if(id ==10){
     sscanf("muminus","%s",name);//-
    }
    if(id ==11){
     sscanf("mu","%s",name);
    }
    if(id ==12){
     sscanf("klong","%s",name);
    }
    if(id ==13){
     sscanf("piplus","%s",name);//+
    }
    if(id ==14){
     sscanf("piminus","%s",name);//-
    }
    if(id ==15){
     sscanf("kplus","%s",name);//+
    }
    if(id ==16){
     sscanf("kminus","%s",name);//-
    }
    if(id ==17){
     sscanf("lambda","%s",name);
    }
    if(id ==18){
     sscanf("lambdabar","%s",name);
    }
    if(id ==19){
     sscanf("kshort","%s",name);
    }
    if(id ==20){
     sscanf("sigmaminus","%s",name);//-
    }
    if(id ==21){
     sscanf("sigmaplus","%s",name);//+
    }
    if(id ==22){
     sscanf("sigmazero","%s",name);//0
    }
    if(id ==23){
     sscanf("pizero","%s",name);//0
    }
    if(id == 24){
     sscanf("kzero","%s",name);//0
    }
    if(id == 25){
     sscanf("kzerobar","%s",name);//0
    }
    if(id ==26){
     sscanf("reserved","%s",name);
    }
    return name;
 }

 int particles_PDG(int id){

 	int pdg;
    if(id == -12){
    pdg = -12;
    }
    if(id == -11){
    pdg = -11;
    }
    if(id == -10){
    pdg = -10;
    }
    if(id == -9){
    pdg = -9;
    }
    if(id == -8){
    pdg = -8;
    }
    if(id == -7){
    pdg = -7;
    }
    if(id == -6){
    pdg = -6;
    }
    if(id == -5){
    pdg = -5;
    }
    if(id == -4){
    pdg = -4;
    }
    if(id == -3){
    pdg = -3;
    }
    if(id == -2){
    pdg = -2;
    }
    if(id == 0){
    pdg = 0;
    }
    if(id == 1){
    pdg = 2212;
    }
    if(id == 2){
    pdg = -2212;
    }
    if(id == 3){
    pdg = 11;
    }
    if(id == 4){
    pdg = -11;
    }
    if(id == 5){
    pdg = 12;
    }
    if(id == 6){
    pdg = -12;
    }
    if(id == 7){
    pdg = 22;
    }
    if(id == 8){
    pdg = 2112;
    }
    if(id == 9){
    pdg = -2112;
    }
    if(id ==10){
    pdg = -13;
    }
    if(id ==11){
    pdg = 13;
    }
    if(id ==12){
    pdg = 130;
    }
    if(id ==13){
    pdg = 211;
    }
    if(id ==14){
    pdg = -211;
    }
    if(id ==15){
    pdg = 321;
    }
    if(id ==16){
    pdg = -321;
    }
    if(id ==17){
    pdg = 3122;
    }
    if(id ==18){
    pdg = -3122;
    }
    if(id ==19){
    pdg = 310;
    }
    if(id ==20){
    pdg = 3112;
    }
    if(id ==21){
    pdg = 3222;
    }
    if(id ==22){
    pdg = 3212;
    }
    if(id ==23){
    pdg = 111;
    }
    if(id == 25){
    pdg = 311;
    }
    if(id == 26){
    pdg = -311;
    }
    if(id ==26){
    pdg = 0;
    }
    return pdg;
 }


   struct SEv{
   	Int_t        event;
   	Int_t 		   hit;
   };


   struct SPm{
  Double_t px;
	Double_t py;
	Double_t pz;
   };

   struct Sen{
    Double_t Enedep;
    Double_t Ekin;
   };

   struct Sposition{
    Double_t    x;
    Double_t    y;
    Double_t    z;

   };

int main(int argc, char **argv) {

  Char_t    name[50];
	Char_t    detector[20];
	Int_t     pdg_code;
	Int_t		  track_id;
	Int_t	   	parent_id;
	SEv		  	Ev;
	SPm 		  Pm;
	Sen			  en;
	Sposition	pos;
	Double_t	time;
  Double_t  global_time;
	Int_t 		id;
  double_t temp1;
  double_t temp2;

   	//TString filename = "fl_prova_prova.root";
   	FILE *fp = fopen(argv[1],"r");
   	TFile *hfile = 0;

   	hfile = TFile::Open(argv[2],"RECREATE");

   	Int_t bsize = 64000;
   	Int_t split = 1;

   	TTree tree("hittree","Fluka simulation pbar annihilation");
    tree.Branch("event",&Ev,"event/I:hit");
    tree.Branch("energy",&en,"Edep/D:Ekin");
    tree.Branch("time",&time,"time/D");
    tree.Branch("global_time",&global_time,"global_time/D");   
    tree.Branch("position",&pos,"x/D:y:z");
    tree.Branch("momentumdir",&Pm,"x/D:y:z");
    tree.Branch("particle",&name,"particle/C");
    tree.Branch("PDG_code",&pdg_code,"PDG_code/I");
    tree.Branch("detector",detector,"detector/C");
    tree.Branch("track_id",&track_id,"track_id/I");
    tree.Branch("parent_id",&parent_id,"parent_id/I");

    string p_name;
   	Int_t count=1;
   	Int_t tracksaved = 1000000;
   	Int_t eventsaved = 1000000;
   	char line [400];
    int i = 0;
    int num;
    string s;

   	while (fgets(line,400,fp)) {
      if(i < 238){
        i++;
        continue;
      }
      else{
      sscanf(&line[0],"%i %i %s %lf %lf %lf %lf %lf %i %i %lf",&Ev.event,&id,detector,&en.Enedep,&en.Ekin,&pos.x,&pos.y,&pos.z,&track_id,&parent_id,&time);
      string temp_detect(detector);
      if(temp_detect.substr(0,3) == "BAR"){
        num = stoi(temp_detect.substr(3, temp_detect.length() - 3));
        num = num - 1;
        s = to_string(num);
        //cout<<num<<
        temp_detect = "Hodor1_" + s;
      };
      if(temp_detect.substr(0,4) == "IBAR"){
        num = stoi(temp_detect.substr(4, temp_detect.length() - 4));        
        num = num - 1;
        s = to_string(num);
        //cout<<num<<" and "<<s<<endl;
        temp_detect = "Hodor0_" + s;
      };
      if(temp_detect.substr(0,3) == "INF"){
        num = stoi(temp_detect.substr(5, temp_detect.length() - 5));        
        num = num - 1;
        s = to_string(num);
        //cout<<num<<" and "<<s<<endl;
        temp_detect = "InnerFibre" + s + "-";
      };
      if(temp_detect.substr(0,3) == "OUT"){
        num = stoi(temp_detect.substr(5, temp_detect.length() - 5));        
        num = num - 1;
        s = to_string(num);
        //cout<<num<<" and "<<s<<endl;
        temp_detect = "OuterFibre" + s + "-";
      };

      strcpy(detector, temp_detect.c_str());

      //convert unities for consistency with Geant4
      en.Enedep = en.Enedep*1000; //GeV to MeV
      en.Ekin = en.Ekin*1000;
//      temp1 = pos.y/100.0; //x and y are swapped to be consistent with a crazy geo 
//      temp2 = pos.x/100.0; //which does not follow the righ hand rule
      pos.x = pos.x*10.; //cm to mm /100.0; //temp1;
      pos.y = pos.y*10.; //cm to mm /100.0; //temp2;
      pos.z = pos.z*10.; //cm to mm /100.0; //cm to m
      Ev.hit = 0;
      Pm.px = 0;
      Pm.py = 0;
      Pm.pz = 0;
      time = time*1E9; //s to ns

      global_time = time + Ev.event*500;
//      en.Ekin = 0;
//      track_id = count;

//      parent_id = 0.;
      p_name = particles_name(id,name);
      pdg_code = particles_PDG(id);
      tree.Fill();

/*
      if(eventsaved != Ev.event){
      count = 1;
      }

      else if(tracksaved != id){
      	count++;
      }


      eventsaved = Ev.event;
      tracksaved = id;
*/
    }
   	}

   tree.Write();

   fclose(fp);
   delete hfile;

   return 0;
}
