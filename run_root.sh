
#!/bin/bash

run_number=$1

rfluka -e amt_geometry -N$(($run_number - 1)) -M$run_number amt_geometry.inp

#rfluka -e fin_geometry -N${run_number} -M1 amt_geometry.inp

run_number=$(printf "%03d" $run_number)

./tree_writer.x amt_geometry${run_number}.log ./amt_sim${run_number}.root

python ./analysis.py amt_sim${run_number}

#rm *${element}00*