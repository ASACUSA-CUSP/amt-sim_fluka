*                                                                      *
*=== mgdraw ===========================================================*
*                                                                      *
      SUBROUTINE MGDRAW ( ICODE, MREG )
        INCLUDE 'dblprc.inc'
        INCLUDE 'dimpar.inc'
        INCLUDE 'iounit.inc'
        INCLUDE 'caslim.inc'
        INCLUDE 'comput.inc'
        INCLUDE 'sourcm.inc'
        INCLUDE 'fheavy.inc'
        INCLUDE 'flkstk.inc'
        INCLUDE 'genstk.inc'
        INCLUDE 'mgddcm.inc'
        INCLUDE 'paprop.inc'
        INCLUDE 'quemgd.inc'
        INCLUDE 'sumcou.inc'
        INCLUDE 'trackr.inc'


*
*----------------------------------------------------------------------*
*                                                                      *
*     Copyright (C) 2003-2019:  CERN & INFN                            *
*     All Rights Reserved.                                             *
*                                                                      *
*                                                                      *
*     MaGnetic field trajectory DRAWing: actually this entry manages   *
*                                        all trajectory dumping for    *
*                                        drawing                       *
*                                                                      *
*     Created on   01 March 1990   by        Alfredo Ferrari           *
*                                              INFN - Milan            *
*                                                                      *
*----------------------------------------------------------------------*
*

c      INCLUDE '(DBLPRC)'
c      INCLUDE '(DIMPAR)'
c      INCLUDE '(IOUNIT)'
c      INCLUDE '(CASLIM)'
c      INCLUDE '(COMPUT)'
c      INCLUDE '(SOURCM)'
c      INCLUDE '(FHEAVY)'
c      INCLUDE '(FLKSTK)'
c      INCLUDE '(GENSTK)'
c      INCLUDE '(MGDDCM)'
c      INCLUDE '(PAPROP)'
c      INCLUDE '(QUEMGD)'
c      INCLUDE '(SUMCOU)'
c      INCLUDE '(TRACKR)'
*
      DIMENSION DTQUEN ( MXTRCK, MAXQMG )
*

      CHARACTER*20 FILNAM
      CHARACTER*8 MRGNAM,NRGNAM
      CHARACTER*20 STPNAME

      DOUBLE PRECISION TIME

C NUMBER OF SUBSTEP FOR DISTRIBUTING EDEP
      INTEGER SUBSTEP
      INTEGER DEPo
      LOGICAL LFCOPE
      SAVE DEPo
      SAVE LFCOPE
      SAVE IRGDET
      SAVE SUBSTEP
      SAVE STPNAME
      DATA LFCOPE / .FALSE. /

*----------------------------------------------------------------------*
*                                                                      *
*     Icode = 1: call from Kaskad                                      *
*     Icode = 2: call from Emfsco                                      *
*     Icode = 3: call from Kasneu                                      *
*     Icode = 4: call from Kashea                                      *
*     Icode = 5: call from Kasoph                                      *
*                                                                      *
*----------------------------------------------------------------------*

C     Store en dep in detec
      CALL GEOR2N ( MREG,   MRGNAM, IERR1 )

c CHECKING IF IN DETECTOR or IN TARGET
      IF (MRGNAM(1:3) .EQ. 'SCI'.OR. MRGNAM .EQ. 'VOID') THEN 
C     & MRGNAM .EQ. 'VOID' .AND. jtrack .EQ. 2 .OR. MRGNAM .EQ. 'FRAME'
C     & .AND. jtrack .EQ. 2) THEN

c CHANGING NAME TO DETECOTR
c we need a string name for the detector a little longer than the the one permitted
c for the Fluka input. We change the name here, before dumping.
        IF(MRGNAM .EQ. 'SCINT') THEN
          STPNAME = 'Scintillator'
        ELSE IF (MRGNAM .EQ. 'VOID') THEN
          STPNAME = 'Void'
        END IF

c SUMMING UP ALL THE ENERGY DEPOSITED IN THE STEP
        IF (MTRACK .GT. 0) THEN
          DEPOSITS = ZERZER
          DO DEPo=1, size(DTRACK)
            DEPOSITS = DEPOSITS + DTRACK(DEPo)
          END DO
        END IF

c DISTRIBUTING ENERGY DEPOSITED IN N SUBSTEP
c if SUBSTEP == 1 the energy deposited in the STEP
c is divided in two and I have two deposition, one at the beginning and one
c in the middle of the step.
c if SUBSTEP == 2 the energy deposited in the STEP is divided by three and I have
c three deposition point, at the beginning at 1/3 and at 2/3
c and so on..

        IF (jtrack == 3 .OR. jtrack == 4) THEN
          SUBSTEP = 1
        ELSE IF (jtrack == 7 .AND. DE .GE. 0.00008) THEN
          SUBSTEP = 1
        ELSE IF (jtrack == 7 .AND. DE .LT. 0.00008) THEN
          SUBSTEP = 1
        ELSE
          SUBSTEP = 5
        END IF

        DE =  DEPOSITS/(SUBSTEP+1)

c COMPUTING EKIN (not really needed for our analysis at this point)

        IF(jtrack .GE. -6) THEN
          mass = AM (jtrack)
        ELSE
          mass = AM (j0track)
        END IF

        EKIN = ETRACK - mass

c DUMPING INFO PER SUBSTEP IF NOT A NEUTRON

        IF (jtrack .NE. 8) THEN
          DO I=0, SUBSTEP

              xpos = (xtrack(0)+(xtrack(1)-xtrack(0))*I/(SUBSTEP+1))
              ypos = (ytrack(0)+(ytrack(1)-ytrack(0))*I/(SUBSTEP+1))
              zpos = (ztrack(0)+(ztrack(1)-ztrack(0))*I/(SUBSTEP+1))


c CONVERTING AGE OF PARTICLE TO NS
c        TIME = atrack(0)/(10.**9)
        

c just shifting the deposition exactly on the edge of the timpix into the detector.
c Needed for the digitization part which consider the deposition on the edge outside
c the detector
c          IF (STPNAME .EQ. 'TpxFront') THEN
c              IF (zpos .LE. 1.23205) THEN
c                zpos = 1.2320501
c              ELSE IF (zpos .GE. 1.28205) THEN
c                zpos = 1.2820499
c              END IF
c           ELSE IF (STPNAME(1:7) .EQ. 'TpxBack') THEN
c              IF (zpos .LE. -1.28205) THEN
c                zpos = -1.2820499
c              ELSE IF (zpos .GE. -1.23205) THEN
c                zpos = -1.2320501
c              END IF
c           ELSE IF (STPNAME .EQ. 'TpxRight') THEN
c            IF (xpos .LE. 1.40805) THEN
c                xpos = 1.4080501
c              ELSE IF (xpos .GE. 1.45805) THEN
c                xpos = 1.4580499
c              END IF
c           ELSE IF (STPNAME .EQ. 'TpxLeft') THEN
c                IF (xpos .LE. -1.45805) THEN
c                    xpos = -1.4080499
c                  ELSE IF (xpos .GE. -1.40805) THEN
c                    xpos = -1.4080501
c                  END IF
c           ELSE IF (STPNAME .EQ. 'TpxBot') THEN
c                IF (ypos .LE. -1.45805) THEN
c                    ypos = -1.4580499
c                  ELSE IF (ypos .GE. -1.40805) THEN
c                    ypos = -1.4080501
c                  END IF
c           ELSE IF (STPNAME .EQ. 'TpxTop') THEN
c                IF (ypos .LE. 1.40805) THEN
c                    ypos = 1.4080501
c                  ELSE IF (ypos .GE. 1.45805) THEN
c                    ypos = 1.4580499
c                  END IF
c          END IF    

c DUMPED INFO: EVENT NUMBER, PARTICLE ID, DETECOTR NAME, ENERGY DEPOSITED, KINETIC ENERGY, POSITION
c               OF ENERGY DEPOSITED, TRACK ID, PARENT TRACK ID.
              WRITE(*,*) NCASE,jtrack,STPNAME,DE,EKIN,xpos,ypos,
     &         zpos,ISPUSR(11),ISPUSR(5),atrack

          END DO
        END IF
      END IF
      RETURN

c      Ntrack,Mtrack,& ,Ttrack(1),,MRGNAM,atrack,DE,EKIN,xtrack(Ntrack),
c      &  ytrack(Ntrack),ztrack(Ntrack)

*
*======================================================================*
*                                                                      *
*     Boundary-(X)crossing DRAWing:                                    *
*                                                                      *
*     Icode = 1x: call from Kaskad                                     *
*             19: boundary crossing                                    *
*     Icode = 2x: call from Emfsco                                     *
*             29: boundary crossing                                    *
*     Icode = 3x: call from Kasneu                                     *
*             39: boundary crossing                                    *
*     Icode = 4x: call from Kashea                                     *
*             49: boundary crossing                                    *
*     Icode = 5x: call from Kasoph                                     *
*             59: boundary crossing                                    *
*                                                                      *
*======================================================================*
*                                                                      *
      ENTRY BXDRAW ( ICODE, MREG, NEWREG, XSCO, YSCO, ZSCO )
      RETURN
*
*======================================================================*
*                                                                      *
*     Event End DRAWing:                                               *
*                                                                      *
*======================================================================*
*                                                                      *
      ENTRY EEDRAW ( ICODE )
c      write(*,*) 'eedraw'
      RETURN
*
*======================================================================*
*                                                                      *
*     ENergy deposition DRAWing:                                       *
*                                                                      *
*     Icode = 1x: call from Kaskad                                     *
*             10: elastic interaction recoil                           *
*             11: inelastic interaction recoil                         *
*             12: stopping particle                                    *
*             13: pseudo-neutron deposition                            *
*             14: escape                                               *
*             15: time kill                                            *
*     Icode = 2x: call from Emfsco                                     *
*             20: local energy deposition (i.e. photoelectric)         *
*             21: below threshold, iarg=1                              *
*             22: below threshold, iarg=2                              *
*             23: escape                                               *
*             24: time kill                                            *
*     Icode = 3x: call from Kasneu                                     *
*             30: target recoil                                        *
*             31: below threshold                                      *
*             32: escape                                               *
*             33: time kill                                            *
*     Icode = 4x: call from Kashea                                     *
*             40: escape                                               *
*             41: time kill                                            *
*             42: delta ray stack overflow                             *
*     Icode = 5x: call from Kasoph                                     *
*             50: optical photon absorption                            *
*             51: escape                                               *
*             52: time kill                                            *
*                                                                      *
*======================================================================*
*                                                                      *
*  +-------------------------------------------------------------------*
*
c Dumping energy and info and stuff also when the tracking is ending

      ENTRY ENDRAW ( ICODE, MREG, RULL, XSCO, YSCO, ZSCO )
      CALL GEOR2N ( MREG,   MRGNAM, IERR1 )

      IF (MRGNAM(1:3) .EQ. 'SCI'.OR. MRGNAM .EQ. 'FOIL' .AND. jtrack .EQ. 2 .OR. MRGNAM .EQ. 'FRAME' .AND. jtrack .EQ. 2) THEN

        IF(MRGNAM(1:3) .EQ. 'SCI') THEN
            STPNAME = 'Scintillator'
        ELSE IF (MRGNAM .EQ. 'VOID') THEN
            STPNAME = 'Void'
        END IF

       IF (STPNAME(1:3) .EQ. 'Sci' .OR. STPNAME .EQ. 'Void') THEN
        
       WRITE(*,*) NCASE,jtrack,STPNAME,RULL,EKIN,xpos,ypos,
     &         zpos,ISPUSR(11),ISPUSR(5),atrack

       END IF
      END IF
      RETURN
*======================================================================*
*                                                                      *
*     SOurce particle DRAWing:                                         *
*                                                                      *
*======================================================================*
*
      ENTRY SODRAW
C      NPRIM = ZERZER
c      WRITE(*,*) NPFLKA,'--sodraw--'
      RETURN
*
*======================================================================*
*                                                                      *
*     USer dependent DRAWing:                                          *
*                                                                      *
*     Icode = 10x: call from Kaskad                                    *
*             100: elastic   interaction secondaries                   *
*             101: inelastic interaction secondaries                   *
*             102: particle decay  secondaries                         *
*             103: delta ray  generation secondaries                   *
*             104: pair production secondaries                         *
*             105: bremsstrahlung  secondaries                         *
*             110: radioactive decay products                          *
*     Icode = 20x: call from Emfsco                                    *
*             208: bremsstrahlung secondaries                          *
*             210: Moller secondaries                                  *
*             212: Bhabha secondaries                                  *
*             214: in-flight annihilation secondaries                  *
*             215: annihilation at rest   secondaries                  *
*             217: pair production        secondaries                  *
*             219: Compton scattering     secondaries                  *
*             221: photoelectric          secondaries                  *
*             225: Rayleigh scattering    secondaries                  *
*             237: mu pair     production secondaries                  *
*     Icode = 30x: call from Kasneu                                    *
*             300: interaction secondaries                             *
*     Icode = 40x: call from Kashea                                    *
*             400: delta ray  generation secondaries                   *
*  For all interactions secondaries are put on GENSTK common (kp=1,np) *
*  but for KASHEA delta ray generation where only the secondary elec-  *
*  tron is present and stacked on FLKSTK common for kp=npflka          *
*                                                                      *
*======================================================================*
*
      ENTRY USDRAW ( ICODE, MREG, XSCO, YSCO, ZSCO )
*
C      IF ( .NOT. LFCOPE ) THEN
C         LFCOPE = .TRUE.
C         IF ( KOMPUT .EQ. 2 ) THEN
C            FILNAM = '/'//CFDRAW(1:8)//' DUMP A'
C         ELSE
C            FILNAM = CFDRAW
C         END IF
C         OPEN ( UNIT = IODRAW, FILE = FILNAM, STATUS = 'NEW', FORM =
C     &          'FORMATTED' )
C      END IF
C      LLOUSE = ICODE

C      write(*,*) '--ic-- ',ICODE
C      IF (LANNRS .EQ. 1) THEN
C         SPAUSR (1) = XSCO
C         SPAUSR (2) = YSCO
C         SPAUSR (3) = ZSCO
C      END IF

C      IF (LANNFL .EQ. 1) THEN
C         SPAUSR (1) = XSCO
C         SPAUSR (2) = YSCO
C         SPAUSR (3) = ZSCO
C      END IF

C      IF (LELEVT .EQ. 1) THEN
C         SPAUSR (1) = XSCO
C         SPAUSR (2) = YSCO
C         SPAUSR (3) = ZSCO
C      END IF

      RETURN
*=== End of subrutine Mgdraw ==========================================*
      END
