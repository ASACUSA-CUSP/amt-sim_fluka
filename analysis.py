#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 29 10:22:22 2024

@author: viktoria
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import ROOT
from array import array
from sklearn.cluster import DBSCAN
import pandas as pd
from cycler import cycler
from matplotlib import colors
from matplotlib import colormaps as cm
import sys

#### plot parameters ####

ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(1)

plt.style.use("ggplot")

plt.set_cmap("viridis")
mcolors.Colormap("viridis").set_bad(color='k', alpha=0)
plt.rcParams["grid.linestyle"] = "--"
plt.rcParams["grid.color"] = "white"# "lightgray"
plt.rcParams["grid.linewidth"] = 0.8
plt.rcParams["axes.axisbelow"] = True

plt.rcParams["font.family"] = "sans-serif"
plt.rcParams.update({'font.size': 14})
plt.rcParams['pdf.fonttype'] = 'truetype'
plt.rcParams["axes.prop_cycle"] = plt.cycler(color=["#398e93", "tab:orange", "#6fc431", "tab:red", "tab:purple", "tab:brown", "tab:pink", "tab:gray", "tab:olive", "tab:cyan"])
plt.rcParams["legend.facecolor"] = "white"
plt.rcParams["savefig.facecolor"] = (1.0, 1.0, 1.0, 0.0)
plt.rcParams["figure.dpi"] = 450

########

def set_darkBG():
    return plt.rcParams.update({
        "text.color": "white",
        'axes.labelcolor': "white",
        "xtick.color": "white",
        "ytick.color": "white",
        "axes.titlecolor": "white",
        "legend.labelcolor": "#555555",
        "figure.facecolor": "#242f35"})
    
def set_whiteBG():
    return plt.rcParams.update({
        "text.color": '#555555',
        'axes.labelcolor': '#555555',
        "xtick.color": '#555555',
        "ytick.color": '#555555',
        "axes.titlecolor": '#555555',
        "legend.labelcolor": "#555555",
        "figure.facecolor": "#ffffff"})


def read_pi_tree(file_name):  # reads ROOT TTree and returns pandas dataframe
    
    tree_name = "hittree"
    file = ROOT.TFile.Open(file_name, "READ")
    tree = file.Get(tree_name)
    
    for ind, event in enumerate(tree):
        if ind == 0:
            data = {"event": [event.event], "track": [event.track_id], "nhits": [event.hit], "x": [event.x], "y": [event.y], 
                    "z": [event.z], "time": [event.time], "global_time": [event.global_time],
                    "detector": [event.detector], "particle": [event.particle], "pdg": [event.PDG_code], "parent": [event.parent_id]}
        elif abs(event.PDG_code) == 211:
            data["event"].append(event.event)
            data["track"].append(event.track_id)
            data["nhits"].append(event.hit)
            data["x"].append(event.x)
            data["y"].append(event.y)
            data["z"].append(event.z)
            data["time"].append(event.time)
            data["global_time"].append(event.global_time)
            data["detector"].append(event.detector)
            data["particle"].append(event.particle)
            data["pdg"].append(event.PDG_code)
            data["parent"].append(event.parent_id)
    file.Close()
    return pd.DataFrame(data)



def read_tree(file_name):  # reads ROOT TTree and returns pandas dataframe
    
    tree_name = "hittree"
    file = ROOT.TFile.Open(file_name, "READ")
    tree = file.Get(tree_name)
    
    for ind, event in enumerate(tree):
        if ind == 0:
            data = {"event": [event.event], "track": [event.track_id], "energy": [event.Edep], 
                    "x": [event.x], "y": [event.y], "z": [event.z], "time": [event.time],
                    "detector": [event.detector], "particle": [event.particle], 
                    "pdg": [event.PDG_code], "nhits": [event.hit], "parent": [event.parent_id]}
        else:
            data["event"].append(event.event)
            data["track"].append(event.track_id)
            data["energy"].append(event.Edep)
            data["x"].append(event.x)
            data["y"].append(event.y)
            data["z"].append(event.z)
            data["time"].append(event.time)
            # data["global_time"].append(event.global_time)
            data["detector"].append(event.detector)
            data["particle"].append(event.particle)
            data["pdg"].append(event.PDG_code)
            data["nhits"].append(event.hit)
            data["parent"].append(event.parent_id)

    file.Close()
    return pd.DataFrame(data)



if len(sys.argv) > 1:
    file_name = str(sys.argv[1])
    print(sys.argv)
    plot = False
else:
    file_name = "amt_sim5"
    plot = True


df = read_tree(file_name+".root")

# df.drop(index = 0, inplace=True)
# df.reset_index(inplace=True)


pdf = df.groupby(["event", "track", "particle", "detector"])[["x", "y", "z", "time", "parent", "pdg"]].first()
pdf["energy"] = df.groupby(["event", "track", "particle"])[["energy"]].sum()
pdf.reset_index(inplace=True)

pidf = pdf[np.abs(pdf.pdg) == 211]
pidf.reset_index(inplace=True, drop=True)

mult_all = []
for ev in pdf.event.unique():
    mult_all.append(len(pdf[(pdf.event == ev) & (pdf.parent == 1) & (abs(pdf.pdg) == 211)].track.unique()))

mult_sci = []
for ev in pdf.event.unique():
    mult_sci.append(len(pdf[(pdf.event == ev) & (pdf.parent == 1) & (abs(pdf.pdg) == 211) & (pdf.detector == "Scintillator")].track.unique()))

pi_prod = np.sum(mult_all)
pi_sci = np.sum(mult_sci)

print("%i pions produced, %i pions measured"%(pi_prod, pi_sci))
print("%4.3f ratio measured/produced"%(pi_sci/pi_prod))

# mult = []
# for ev in df.event.unique():
#     mult.append(len(df[(df.event == ev) & (df.parent == 1)].track.unique()))
pis = plt.hist(mult_all, bins=np.arange(8), align="left", rwidth=0.8)
plt.xlabel("produced charged pions")
plt.xticks(np.arange(8), np.arange(8))
plt.legend(["%i pions"%pi_prod])
plt.savefig(file_name+"_pion_mult_prod.pdf", dpi=450, bbox_inches="tight", pad_inches = 0.1)
if plot:
    plt.show()
else:
    plt.close()


sci = plt.hist(mult_sci, bins=np.arange(8), align="left", rwidth=0.8)
plt.xlabel("measured charged pions")
plt.xticks(np.arange(8), np.arange(8))
plt.legend(["%i pions"%pi_sci])
plt.savefig(file_name+"_pion_mult_meas.pdf", dpi=450, bbox_inches="tight", pad_inches = 0.1)
if plot:
    plt.show()
else:
    plt.close()


pidf["ang"] = np.arctan2(pidf.y, pidf.x)

gap = np.min(np.abs(pidf.ang[pidf.detector == "Scintillator"]))
seg = (np.pi-2*gap)/4

posbins = np.array([gap-0.1, gap+seg, gap+2*seg, gap+3*seg, gap+4*seg+0.1])
negbins = -posbins

posnames = ["NE", "NNE", "NNW", "NW"]
negnames = ["SE", "SSE", "SSW", "SW"]

scipi = pidf[pidf.detector == "Scintillator"]
scipi.reset_index(inplace=True, drop=True)


segment = []
for pi in scipi.index:
    try:
        if scipi.iloc[pi].y > 0:
            # for i in np.arange(4):
            #     if posbins[i] >= scipi.iloc[pi].ang < posbins[i+1]:
            #         segment.append(posnames[i])
            segment.append(posnames[(np.digitize(scipi.iloc[pi].ang, posbins,  right=True))-1])
        elif scipi.iloc[pi].y < 0:
            # for i in np.arange(4):
            #     if negbins[i] >= scipi.iloc[pi].ang < negbins[i+1]:
            #         segment.append(negnames[i])
            segment.append(negnames[(np.digitize(scipi.iloc[pi].ang, negbins,  right=False))-1])
    except IndexError:
        print(pi)
        
scipi["segment"] = segment


hits = []
for ev in scipi.event.unique():
    hits.append(len(scipi[scipi.event == ev].segment.unique()))


hits2 = []
for ev in scipi.event.unique():
    hits2.append(len(scipi[(scipi.event == ev) & (scipi.segment != "NW")].segment.unique()))


seghits = plt.hist(hits, bins=np.arange(1, 8), align="left", rwidth=0.8)
plt.xlabel("segments hit")
plt.xticks(np.arange(1, 8), np.arange(1, 8))
plt.legend(["2-coin/3-coin = %4.3f"%(seghits[0][1]/seghits[0][2])])
plt.savefig(file_name+"_segments_hit.pdf", dpi=450, bbox_inches="tight", pad_inches = 0.1)
if plot:
    plt.show()
else:
    plt.close()


seghits2 = plt.hist(hits2, bins=np.arange(1, 8), align="left", rwidth=0.8)
plt.xlabel("segments hit")
plt.xticks(np.arange(1, 8), np.arange(1, 8))
plt.legend(["2-coin/3-coin = %4.3f"%(seghits2[0][1]/seghits2[0][2])])
plt.savefig(file_name+"_segments_hit_1broken.pdf", dpi=450, bbox_inches="tight", pad_inches = 0.1)
if plot:
    plt.show()
else:
    plt.close()



# sci[0][2]/sci[0][3]
# pis[0][2]/pis[0][3]

# plt.hist2d(pidf[pidf.detector == "Scintillator"].x, pidf[pidf.detector == "Scintillator"].y, bins=40)
# plt.colorbar()

# # plt.scatter(pdf.x, pdf.y, c=pdf.z)
# # plt.scatter(pdf.x, pdf.z, c=pdf.y)

# plt.hist2d(pidf[pidf.detector == "Scintillator"].z, pidf[pidf.detector == "Scintillator"].y, bins=40)

# plt.scatter(pidf.x, pidf.y, c=pidf.ang)

# file = ROOT.TFile.Open("amt_sim.root", "READ")
# tree = file.Get("hittree")

# d = ROOT.RDataFrame("hittree", "amt_sim.root"); 
# ´