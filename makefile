LIBS = `root-config --libs`
INCS = `root-config --cflags`

# this was suggested by ChatGPT as I ran into some compiling issues with linking functions
GLIBC_FLAGS = -Wl,--unresolved-symbols=ignore-all -Wl,--no-as-needed


TARGET_EXECUTABLES = tree_writer.x
obj = tree_writer.o

all : $(TARGET_EXECUTABLES);

tree_writer.x : tree_writer.o
	c++  -o $@ tree_writer.o $(LIBS) $(INCS) $(GLIBC_FLAGS)
tree_writer.o : tree_writer.c
	c++ -std=c++17 -c tree_writer.c $(LIBS) $(INCS) $(GLIBC_FLAGS)

clean :
	rm *.o