## Simulation of the AMT Scintillator

Created for FLUKA 4-3.4 and Flair 3.3-0.3 (also needs ROOT and Python 3)

Once FLUKA/Flair is installed you can just run: 

ldpmqmd -m fluka -o amt_geometry my_mgdraw.f my_stf.f my_ste.f

and then:

bash ./run_root.sh X

where X is the run number you want to give.
Also automatically produces plots with python showing the multiplicities and hit segments.
