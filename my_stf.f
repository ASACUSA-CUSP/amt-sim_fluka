*$ CREATE STUPRF.FOR
*COPY STUPRF
*
*=== stuprf ===========================================================*
*
      SUBROUTINE STUPRF ( IJ, MREG, XX, YY, ZZ, NPSECN, NPPRMR )

c These includes are for running with FLUKA CERN
       INCLUDE 'dblprc.inc'
       INCLUDE 'dimpar.inc'
       INCLUDE 'iounit.inc'
       INCLUDE 'evtflg.inc'
       INCLUDE 'flkstk.inc'
       INCLUDE 'fheavy.inc'
       INCLUDE 'genstk.inc'
       INCLUDE 'paprop.inc'
       INCLUDE 'resnuc.inc'
       INCLUDE 'trackr.inc'


*
*----------------------------------------------------------------------*
*                                                                      *
*     Copyright (C) 1997-2005      by    Alfredo Ferrari & Paola Sala  *
*     All Rights Reserved.                                             *
*                                                                      *
*                                                                      *
*     SeT User PRoperties for Fluka particles:                         *
*                                                                      *
*     Created on  09 october 1997  by    Alfredo Ferrari & Paola Sala  *
*                                                   Infn - Milan       *
*                                                                      *
*     Last change on  14-jul-05    by    Alfredo Ferrari               *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
*     modified Nov 2019 by fluka course teachers
*
c These includes are for running with FLUKA INFN
c      INCLUDE '(DBLPRC)'
c      INCLUDE '(DIMPAR)'
c      INCLUDE '(IOUNIT)'
c      INCLUDE '(EVTFLG)'
c      INCLUDE '(PAPROP)'
c      INCLUDE '(RESNUC)'
c      INCLUDE '(FHEAVY)'
c      INCLUDE '(FLKSTK)'
c      INCLUDE '(GENSTK)'
c      INCLUDE '(TRACKR)'

*
      INTEGER PARENT
      COMMON /FOLLOW/  THRESH, NPHOT, ICINT
*
      SAVE PARENT

      LOUSE   (NPFLKA)  = LLOUSE
      DO 100 ISPR = 1, MKBMX1
         SPAREK (ISPR,NPFLKA) = SPAUSR (ISPR)
  100 CONTINUE
      DO 200 ISPR = 1, MKBMX2
         ISPARK (ISPR,NPFLKA) = ISPUSR (ISPR)
  200 CONTINUE
*  Increment the track number and put it into the last flag:
      IF ( NPSECN .GT. NPPRMR ) THEN
         IF ( NTRCKS .EQ. 2000000000 ) NTRCKS = -2000000000
         NTRCKS = NTRCKS + 1
         ISPARK (MKBMX2,NPFLKA) = NTRCKS
      END IF


C KEEPING TRACK OF PARENT ID
C            PARENT=NPFLKA-1
C            IF (ISPUSR(8) .EQ. NPFLKA) THEN
C              ISPARK(5,NPFLKA) = ISPARK(5,PARENT)
C            ELSE
C              ISPARK (5,NPFLKA) = ISPUSR(11)
C            END IF
C
C            ISPARK (8,NPFLKA) = NPFLKA

            IF (ISPARK (11,NPFLKA) .EQ. ISPUSR(11)) THEN
                  ISPARK (5,NPFLKA) = ISPUSR(5)
            ELSE 
                  ISPARK (5,NPFLKA) = ISPUSR(11)
            ENDIF

      RETURN
*=== End of subroutine Stuprf =========================================*
      END
